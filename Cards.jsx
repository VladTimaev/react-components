import React from "react";
import { Card, Row } from "react-bootstrap";
import icon from "../icons/Icon3.png";
import icon_second from "../icons/Icon2.png";
import icon_third from "../icons/Icon.png";


function Cards() {
  return (
    <div>
    <Row className="cards-content">
    <Card style={{ width: '18rem' }} className="card-list">
    <Card.Body>
    <img src={icon} alt="icon"/>
    <Card.Title>
    Preset List of Task
    </Card.Title>
    <Card.Text>
    Make bill payments easily using the wallet app
    </Card.Text>
    </Card.Body>
    </Card>
    <Card style={{ width: '18rem' }} className="card-reminder">
    <Card.Body>
    <img src={icon_second} alt="icon"/>
    <Card.Title>
    Reminder of Task
    </Card.Title>
    <Card.Text>
    Make bill payments easily using the wallet app
    </Card.Text>
    </Card.Body>
    </Card>
    <Card style={{ width: '18rem' }} className="card-report">
    <Card.Body>
    <img src={icon_third} alt="icon"/>
    <Card.Title>
    Complecation Report
    </Card.Title>
    <Card.Text>
    Make bill payments easily using the wallet app
    </Card.Text>
    </Card.Body>
    </Card>
    </Row>
    </div>
  );
}

export default Cards;
