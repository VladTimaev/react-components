import React from "react";
import { Container, Button, Row, Col} from "react-bootstrap";
import mobile from "../images/mobile-telephone.png";

function Partners () {
  return (
    <div>
    <Container>
      <Row className="title-app">
       <Col>
       <h1>Get it now for free</h1>
        <p>We are offering free debit cards once you sing up and order a card. we wont chargeyou for our debit card.</p>
          <Button className="app-button" variant="light">
           <p>Download on the</p>
          </Button>
          <Button variant="light">
          <p>GET IT ON</p>
          </Button>
         </Col>
         <Col>
         <img src={mobile} alt="mobile"/>
         </Col>
      </Row>
    </Container>
    </div>
  );
}

export default Partners;
