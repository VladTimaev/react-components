import React from "react";
import {Container, Card, ListGroup, ListGroupItem, Row, Button, Form} from "react-bootstrap"
import data from "../data/dataFile";


function CardBlock () {

const [count, setCount] = React.useState(0);

function handleClick () {
  setCount(count + 1)
};

const [counter, setCounter] = React.useState(30);

function counterClick () {
  setCounter(counter + 1)
};

  return (
    <>
    <Container>
    <Row className="title">
      <h1>{data[3].title}</h1>
      <p>{data[3].content}</p>
    </Row>
      <Row>
       <div className="custom-switch">
       <h1>Monthly</h1>
        <Form>
        <Form.Check
        type="switch"
        id="custom-switch"
        />
        </Form>
       <h1>Yearly</h1>
       </div>
       </Row>
      <Row className="card--title">
      <Card className="card--body">
       <Card.Body>
        <h1>Freee</h1>
        <ListGroup className="list-group-flush">
          <li>1 seat</li>
          <li>2 project</li>
          <ListGroupItem>
          <div className="title-number">
          <p>$</p>
          <h2>{count}</h2>
          <p>/Forever</p>
          </div>
          </ListGroupItem>
           </ListGroup>
         <Button onClick={handleClick} variant="light">Start 14 Days Trial</Button>
        </Card.Body>
              </Card>
              <Card className="card--body card-second">
            <Card.Body>
           <h1>Premium</h1>
           <ListGroup className="list-group-flush list-group">
       <li>Unlimited Seat</li>
       <li>Unlimited project</li>
       <ListGroupItem>
       <div className="title-number">
       <p>$</p>
       <h2>{counter}</h2>
       <p>/Forever</p>
       </div>
       </ListGroupItem>
     </ListGroup>
      <Button onClick={counterClick} variant="light">Start 14 Days Trial</Button>
   </Card.Body>
</Card>
 </Row>
    </Container>
    </>
  );
};

export default CardBlock;
