import React from "react";
import {Container, Row, Col, Card} from "react-bootstrap";
import mobileScreen from "../images/Objects.png";
import data from "../data/dataFile";
import iconFirst from "../icons/iconFirst.png";
import iconSecond from "../icons/iconSecond.png";
import iconThird from "../icons/iconThird.png";
import Cards from "../components/Cards";

function Analytics() {
  return (
    <div>
    <Container>
    <Row className="row"  xs={2} md={4} lg={6}>
    <Col >
    <h4>15k+</h4>
    <p>Active user</p>
    </Col>
    <Col>
    <h4>30k</h4>
    <p>Total Download</p>
    </Col>
    <Col>
    <h4>10k</h4>
    <p>Customer</p>
    </Col>
    </Row>
    <Row className="title">
    <h1>{data[0].title}</h1>
    <p>{data[0].content}</p>
    </Row>
    <Cards />
    </Container>
    <Container className="container-cards">
    <Row>
    <div className="col-6 mobile-screen">
    <img src={mobileScreen} alt="mobile-screen"/>
    </div>
    <div className="col-6 col-cards">
    <h1>{data[1].title}</h1>
    <h1>{data[1].headline}</h1>
    <p className="content-head">{data[1].content}</p>
    <Card className="card-list">
    <Card.Body>
    <div className="title-modal">
    <img src={iconFirst} alt="icon"/>
    <Col className="title-content">
    <h5>Automatic event tracking</h5>
    <p>Time Tracking is never been easier.</p>
    <p>Just let the stopwatch run</p>
    </Col>
    </div>
    </Card.Body>
    </Card>
    <Card className="card-list">
    <Card.Body>
    <div className="title-modal">
    <img src={iconSecond} alt="icon"/>
    <Col className="title-content">
    <h5>Automatic event tracking</h5>
    <p>Time Tracking is never been easier.</p>
    <p>Just let the stopwatch run</p>
    </Col>
    </div>
    </Card.Body>
    </Card>
    <Card className="card-list">
    <Card.Body>
    <div className="title-modal">
    <img src={iconThird} alt="icon"/>
    <Col className="title-content">
    <h5>Automatic event tracking</h5>
    <p>Time Tracking is never been easier.</p>
    <p>Just let the stopwatch run</p>
    </Col>
    </div>
    </Card.Body>
    </Card>
    </div>
    </Row>
    </Container>
    </div>
  );
}

export default Analytics;
