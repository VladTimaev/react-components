import React from "react";
import {Navbar, Container, Nav, Button, Row, Col} from "react-bootstrap";
import logo from "../icons/Logo.png";
import instagram from "../icons/Instagram.png";
import linkedin from "../icons/Linkedin.png";
import facebook from "../icons/Facebook.png";
import twitter from "../icons/Twitter.png";

function Footer() {
  return (
    <div>
    <Container>
    <footer>
    <Navbar bg="white" variant="dark">
      <Container>
      <Navbar.Brand href="#home">
      <img src={logo} alt="logo"/>
      </Navbar.Brand>
      <Nav className="me-auto">
        <Nav.Link href="#home" style={{color: "#1C0E0D" }}>Demos</Nav.Link>
        <Nav.Link href="#features"style={{color: "#1C0E0D" }}>Features</Nav.Link>
        <Nav.Link href="#pricing" style={{color: "#1C0E0D" }}>Pricing</Nav.Link>
        <Nav.Link href="#pricing" style={{color: "#1C0E0D" }}>Contact</Nav.Link>
      </Nav>
      </Container>
      <Nav className="Footer">
        <Nav.Link href="#home">
        <img src={instagram} alt="instagram"/>
        </Nav.Link>
        <Nav.Link href="#features">
        <img src={linkedin} alt="linkedin"/>
        </Nav.Link>
        <Nav.Link href="#pricing">
        <img src={facebook} alt="facebook"/>
        </Nav.Link>
        <Nav.Link href="#pricing">
        <img src={twitter} alt="twitter"/>
        </Nav.Link>
      </Nav>
    </Navbar>
    <Row>
    <hr/>
    <div className="footer-paragraph">
      <p>Terms & condition</p>
      <p>Privacy policy</p>
      <p>All Right Reserved @ plowv.com</p>
     </div>
    </Row>
    </footer>
    </Container>
    </div>
  );
}

export default Footer;
