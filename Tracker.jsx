import React from "react";
import {Container, Row, Card, Col} from "react-bootstrap";
import iconTime from "../icons/Icontimer.png";
import iconexpenses from "../icons/Iconexpenses.png";
import iconwallet from "../icons/Iconwallet.png";
import data from "../data/dataFile";

function Tracker () {
  return (
    <div className="container-tracker">
    <Container>
      <Row>
       <h1>How our Tracker work for you</h1>
       <p>An enim nullam tempor sapien gravida donec enim ipsum porta justo  congue magna at pretium purus pretium ligula </p>
      </Row>
       <Card className="card-list">
        <Card.Body>
         <div className="title-modal">
         <img src={iconTime} alt="icon"/>
         <Col className="title-content">
           <h5>Automatic event tracking</h5>
            <p>Time Tracking is never been easier.</p>
            <p>Just let the stopwatch run</p>
          </Col>
        </div>
        </Card.Body>
      </Card>
        <Card className="card-list-time card-list">
      <Card.Body>
   <div className="title-modal">
<img src={iconexpenses} alt="icon"/>
  <Col className="title-content">
    <h5>Automatic event tracking</h5>
     <p>Time Tracking is never been easier.</p>
      <p>Just let the stopwatch run</p>
  </Col>
    </div>
    </Card.Body>
     </Card>
       <Card className="card-list">
        <Card.Body>
         <div className="title-modal">
         <img src={iconwallet} alt="icon"/>
        <Col className="title-content">
         <h5>Automatic event tracking</h5>
         <p>Time Tracking is never been easier.</p>
         <p>Just let the stopwatch run</p>
        </Col>
      </div>
    </Card.Body>
  </Card>
</Container>
</div>
  );
}

export default Tracker;
