import React from "react";
import {Navbar, Container, Nav, Button} from "react-bootstrap";
import logo from "../icons/Logo.png";
import '../App.scss';

function Header () {
  return (
    <div>

  <Navbar bg="white" variant="dark">
    <Container>
    <Navbar.Brand href="#home">
    <img src={logo} alt="logo"/>
    </Navbar.Brand>
    <Nav className="me-auto">
      <Nav.Link href="#home" style={{color: "#1C0E0D" }}>Demos</Nav.Link>
      <Nav.Link href="#features"style={{color: "#1C0E0D" }}>Features</Nav.Link>
      <Nav.Link href="#pricing" style={{color: "#1C0E0D" }}>Pricing</Nav.Link>
      <Nav.Link href="#pricing" style={{color: "#1C0E0D" }}>Contact</Nav.Link>
    </Nav>
    <Button className="button-login" variant="white">Login</Button>
    <Button className="button-sign" variant="white">Sign up</Button>
    </Container>
  </Navbar>

    </div>
  );
}

export default Header;
