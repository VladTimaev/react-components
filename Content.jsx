import React from "react";
import { Container, Button } from "react-bootstrap";
import Header from "./Header";

function Banner() {
  return (
    <div>
    <Container className="banner">
    <Header />
    <div className="banner-heading">
    <h1>A collaborative  Time Tracking that you Need</h1>
    <p>An enim nullam tempor sapien gravida donec enim ipsum porta justo  congue magna at pretium purus pretium ligula </p>
    <Button className="button-content" variant="white">Start 14 Days Trial</Button>
    </div>
    </Container>
    </div>
  );
}

export default Banner;
